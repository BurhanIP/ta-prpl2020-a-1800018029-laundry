<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,500,500i,600,600i,700,700i&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
 
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/style.css">
			<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
     <script src="js/jquery-3.2.1.js"></script>
    <script src="js/bootstrap.min.js"></script>


    <style type="text/css">
    	body{
    		padding: 0;
    		margin:0;
    		background-image: url('images/basdat.jpg');
    		background-size: cover;
    		background-repeat: no-repeat;

    	}
    	* {box-sizing: border-box}


    </style>
	
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<div class="container">
			
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link" href="index.php"><i class="fa fa-home"></i>&nbspHome</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="insert-data.php"><i class="fa fa-plus "></i>&nbsp Tambah Data Pelanggan</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="insert-data2.php"><i class="fa fa-plus "></i>&nbsp Tambah Data Pegawai</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="insert-data3.php"><i class="fa fa-plus "></i>&nbsp Tambah Data Transaksi</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="see-data.php"><i class="fa fa-file "></i>&nbsp Lihat Semua Data</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>

    <section class="ftco-section bg-light">
    	<div class="container">
			<h2 align="center">SILAHKAN PILIH PAKET LAUNDRY</h2>
		</div>
				<div class="row no-gutters">
    			<div class="col-lg-6">
    				<div class="room-wrap d-md-flex">
    					<a href="2" class="img" style="background-image: url(images/cuci.jpg);"></a>
    					<div class="half left-arrow d-flex align-items-center">
    						<div class="text p-4 p-xl-5 text-center">
    							<p class="star mb-0"><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></p>
    							<!-- <p class="mb-0"><span class="price mr-1">$120.00</span> <span class="per">per night</span></p> -->
	    						<h3 class="mb-3"><a href="rooms.html">Laundry Kilat</a></h3>
	    						<ul class="list-accomodation">
	    							<li><span>1 hari jadi</li>
	    							<li><span>Free Delivery</li>
	    							<li><span>Cuci + Setrika</li>
	    							<li><span>8000/kg</li>
	    						</ul>
	    						<p class="pt-1"><a href="room-single.html" class="btn-custom px-3 py-2">View Details <span class="icon-long-arrow-right"></span></a></p>
    						</div>
    					</div>
    				</div>
    			</div>
    			<div class="col-lg-6">
    				<div class="room-wrap d-md-flex">
    					<a href="#" class="img" style="background-image: url(images/mesin.jpg);"></a>
    					<div class="half left-arrow d-flex align-items-center">
    						<div class="text p-4 p-xl-5 text-center">
    							<p class="star mb-0"><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></p>
    							<!-- <p class="mb-0"><span class="price mr-1">$120.00</span> <span class="per">per night</span></p> -->
	    						<h3 class="mb-3"><a href="rooms.html">Laundry Reguler</a></h3>
									<ul class="list-accomodation">
	    							<li><span>3 hari jadi</li>
	    							<li><span>Free Delivery</li>
	    							<li><span>Cuci + Setrika</li>
	    							<li><span>4000/kg</li>
	    						</ul>
	    						<p class="pt-1"><a href="room-single.html" class="btn-custom px-3 py-2">View Details <span class="icon-long-arrow-right"></span></a></p>
    						</div>
    					</div>
    				</div>
    			</div>

    			<div class="col-lg-6">
    				<div class="room-wrap d-md-flex">
    					<a href="#" class="img order-md-last" style="background-image: url(images/cuci1.jpg);"></a>
    					<div class="half right-arrow d-flex align-items-center">
    						<div class="text p-4 p-xl-5 text-center">
    							<p class="star mb-0"><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></p>
    							<!-- <p class="mb-0"><span class="price mr-1">$120.00</span> <span class="per">per night</span></p> -->
	    						<h3 class="mb-3"><a href="rooms.html">Cuci</a></h3>
									<ul class="list-accomodation">
	    							<li><span>3 hari jadi</li>
	    							<li><span>Free Delivery</li>
	    							<li><span>Cuci saja</li>
	    							<li><span>3000/kg</li>
	    						</ul>
	    						<p class="pt-1"><a href="room-single.html" class="btn-custom px-3 py-2">View Details <span class="icon-long-arrow-right"></span></a></p>
    						</div>
    					</div>
    				</div>
    			</div>
    			<div class="col-lg-6">
    				<div class="room-wrap d-md-flex">
    					<a href="#" class="img order-md-last" style="background-image: url(images/7.jpg);"></a>
    					<div class="half right-arrow d-flex align-items-center">
    						<div class="text p-4 p-xl-5 text-center">
    							<p class="star mb-0"><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></p>
    							<!-- <p class="mb-0"><span class="price mr-1">$120.00</span> <span class="per">per night</span></p> -->
	    						<h3 class="mb-3"><a href="rooms.html">Setrika</a></h3>
									<ul class="list-accomodation">
	    							<li><span>3 hari jadi</li>
	    							<li><span>Free Delivery</li>
	    							<li><span>Setrika saja</li>
	    							<li><span>3000/kg</li>
	    						</ul>
	    						<p class="pt-1"><a href="room-single.html" class="btn-custom px-3 py-2">View Details <span class="icon-long-arrow-right"></span></a></p>
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
			</div>
		</section>
	


	
</body>
</html>