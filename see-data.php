<html>
<head>

			<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
     <script src="js/jquery-3.2.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<div class="container">
			
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link" href="index.php"><i class="fa fa-home"></i>&nbspHome</a>
					</li>
					<li class="nav-item">
						<a class="nav-link"  href="insert-data.php"><i class="fa fa-plus "></i>&nbsp Tambah Data Pelanggan</a>
					</li>
					<li class="nav-item">
						<a class="nav-link"  href="insert-data2.php"><i class="fa fa-plus "></i>&nbsp Tambah Data Pegawai</a>
					</li>
					<li class="nav-item">
						<a class="nav-link"  href="insert-data3.php"><i class="fa fa-plus "></i>&nbsp Tambah Data Transaksi</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	</body>

<?php
	include "koneksi.php";


	$data = mysqli_query($koneksi,"SELECT * FROM pelanggan");
	$no = 1;
?>
<div class="container" style="margin-top:20px">
		<h4>DATA PELANGGAN</h4>
		<hr>
<div class="table-responsive   table-sm ">
  <table class="table">

	<tr>
		<th>No</th>
		<th>ID PELANGGAN</th>
		<th>NAMA PELANGGAN</th>
		<th>NO HP</th>
		<th>ALAMAT PELANGGAN</th>
		<th>AKSI</th>
	</tr>

<?php

foreach ($data as $x) { ?>

	<tr>
		<td><?= $no?></td>	
		<td><?= $x['id_pelanggan']?></td>
		<td><?= $x['nama_pelanggan']?></td>
		<td><?= $x['no_hp']?></td>
		<td><?= $x['alamat_pelanggan']?></td>
		<td>
			<a href="edit.php?id_pelanggan=<?php echo $x['id_pelanggan'];?>"><i class="fa fa-edit" title="Edit Data"></i></a>&nbsp&nbsp&nbsp&nbsp
			<a href="delete.php?id_pelanggan=<?php echo $x['id_pelanggan'];?>"><i class="fa fa-trash" title="Hapus Data"></i>
	</tr>

<div>

<?php
$no++; } 
?>
</div>

</table>
<?php
	include "koneksi.php";


	$data = mysqli_query($koneksi,"SELECT * FROM pegawai");
	$no = 1;
?>

<div class="table-responsive   table-sm ">

  <table class="table">
<div class="container" style="margin-top:20px">
		<h5>DATA PEGAWAI</h5>
		
	<tr>
		<th>No</th>
		<th>ID Pegawai</th>
		<th>NAMA Pegawai</th>
		<th>NO HP</th>
		<th>ALAMAT Pegawai</th>
		<th>AKSI</th>
	</tr>

<?php

foreach ($data as $x) { ?>

	<tr>
		<td><?= $no?></td>	
		<td><?= $x['id_pegawai']?></td>
		<td><?= $x['nama_pegawai']?></td>
		<td><?= $x['no_hp']?></td>
		<td><?= $x['alamat_pegawai']?></td>
		<td>
			<a href="2edit.php?id_pegawai=<?php echo $x['id_pegawai'];?>"><i class="fa fa-edit" title="Edit Data"></i></a>&nbsp&nbsp&nbsp&nbsp
			<a href="delete2.php?id_pegawai=<?php echo $x['id_pegawai'];?>"><i class="fa fa-trash" title="Hapus Data"></i>
	</tr>

<div>

<?php
$no++; } 
?>
</div>

</table>


<?php
	include "koneksi.php";


	$data = mysqli_query($koneksi,"SELECT * FROM transaksi");
	$no = 1;
?>
<div class="container" style="margin-top:20px">
		<h6>DATA TRANSAKSI</h6>
		<hr>
<div class="table-responsive   table-sm ">
  <table class="table">

	<tr>
		<th>No</th>
		<th>ID TRANSAKSI</th>
		<th>TANGGAL</th>
		<th>PAKET LAYANAN</th>
		<th>BERAT PAKAIAN</th>
		<th>TOTAL HARGA</th>
		<th>AKSI</th>
	</tr>

<?php

foreach ($data as $x) { ?>

	<tr>
		<td><?= $no?></td>	
		<td><?= $x['id_transaksi']?></td>
		<td><?= $x['tanggal_transaksi']?></td>
		<td><?= $x['paket_layanan']?></td>
		<td><?= $x['berat_pakaian']?></td>
		<td><?= $x['total_harga']?></td>
		<td>
			<a href="3edit.php?id_transaksi=<?php echo $x['id_transaksi'];?>"><i class="fa fa-edit" title="Edit Data"></i></a>&nbsp&nbsp&nbsp&nbsp
			<a href="delete3.php?id_transaksi=<?php echo $x['id_transaksi'];?>"><i class="fa fa-trash" title="Hapus Data"></i>
	</tr>

<div>

<?php
$no++; } 
?>
</div>
</table>
  